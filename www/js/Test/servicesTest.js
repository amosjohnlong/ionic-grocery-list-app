describe('GroceryListApp', function() {
	
	beforeEach(module('starter.controllers'));
	
	describe('localRepositoryService', function() {
		it("saveToRepository test", inject(['localRepositoryService', function(localRepositoryService) {
			expect(localRepositoryService).toBeDefined();
			
			var key = 'Some random key';
			var value = 'Some random value';
			
			var promise = localRepositoryService.saveToRepository(key, value);
			expect(promise).toBeDefined();
			expect(promise.$$state.status).toEqual(1);
		}]));
		
		it("retrieveFromRepository test", inject(['localRepositoryService', function(localRepositoryService) {
			expect(localRepositoryService).toBeDefined();
			
			var key = 'Another random key';
			var value = 'Another random value';
			localRepositoryService.saveToRepository(key, value);
			
			var actual = localRepositoryService.retrieveFromRepository(key);
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(value);
		}]));
	});
	
	describe('cookieRepositoryService', function() {
		var cookieValue;
		var mockCookies = {
			put : function(key, value, extra) { cookieValue = value; },
			get : function(key) { return cookieValue; }
		}
		
		beforeEach(function () {
			module(function ($provide) {
				$provide.value('$cookies', mockCookies);
			});
		});
	
		it("saveToRepository test", inject(['cookieRepositoryService', function(cookieRepositoryService) {
			expect(cookieRepositoryService).toBeDefined();
			
			var key = 'Some random key';
			var value = 'Some random value';
			
			var promise = cookieRepositoryService.saveToRepository(key, value);
			expect(promise).toBeDefined();
			expect(promise.$$state.status).toEqual(1);
		}]));
				
		it("retrieveFromRepository test", inject(['cookieRepositoryService', function(cookieRepositoryService) {
			expect(cookieRepositoryService).toBeDefined();
			
			var key = 'Another random key';
			var value = 'Another random value';
			cookieRepositoryService.saveToRepository(key, value);
			
			var actual = cookieRepositoryService.retrieveFromRepository(key);
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(value);
		}]));
	});
	
	describe('groceryListService', function() {
		var groceryList = [{'id': 1, 'name': 'Turkey', 'hasGotten': false }];
		var savedGroceryList;
		var $scope;
		
		beforeEach(function () {
			module(function($provide){
				$provide.service('cookieRepositoryService', ['$q', function($q) {
					return { 
						retrieveFromRepository: function(data) { 
							return $q.resolve(groceryList); 
						}, 
						saveToRepository: function(a, newGroceryList, c) { 
							savedGroceryList = newGroceryList;
							return $q.resolve(); 
						}
					};
				}]);
			});
		});
        
		beforeEach(inject(function(_$rootScope_, _$q_) {
			$scope = _$rootScope_.$new();
		}));
		
		it("getGroceryList test", inject(['groceryListService', function(groceryListService) {
			expect(groceryListService).toBeDefined();
			var key = 'grocery key';
			
			var actual = groceryListService.getGroceryList(key);
			$scope.$apply();
			
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(groceryList);
		}]));
				
		it("addItemToList test - Add to Empty List", inject(['groceryListService', function(groceryListService) {
			expect(groceryListService).toBeDefined();
			
			var newGroceryList = [];
			var newItem1 = {id: 1, 'name': 'Canola Oil', 'hasGotten': false };
			newGroceryList.push(newItem1);
			
			var actual = groceryListService.addItemToList(newItem1.name);
			$scope.$apply();
			
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(true);
			expect(newGroceryList).toEqual(savedGroceryList);
		}]));
		
		it("addItemToList test - Add to Non-Empty List", inject(['groceryListService', function(groceryListService) {
			expect(groceryListService).toBeDefined();
			
			var newGroceryList = [];
			var newItem1 = {id: 1, 'name': 'Canola Oil', 'hasGotten': false };
			var newItem2 = {id: 2, 'name': 'Asparagus', 'hasGotten': false };
			newGroceryList.push(newItem1);
			newGroceryList.push(newItem2);
			
			groceryListService.addItemToList(newItem1.name);
			var actual = groceryListService.addItemToList(newItem2.name);
			$scope.$apply();
			
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(true);
			expect(newGroceryList).toEqual(savedGroceryList);
		}]));
		
		it("removeItemFromList test - First Item", inject(['groceryListService', function(groceryListService) {
			expect(groceryListService).toBeDefined();
			
			var newGroceryList = [];
			var newItem1 = {id: 1, 'name': 'Canola Oil', 'hasGotten': false };
			var newItem2 = {id: 2, 'name': 'Asparagus', 'hasGotten': false };
			var newItem3 = {id: 3, 'name': 'Pulled Pork', 'hasGotten': false };
			newGroceryList.push(newItem2);
			newGroceryList.push(newItem3);
			
			groceryListService.addItemToList(newItem1.name);
			groceryListService.addItemToList(newItem2.name);
			groceryListService.addItemToList(newItem3.name);
			var actual = groceryListService.removeItemFromList(newItem1.id);
			$scope.$apply();
			
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(true);
			expect(newGroceryList).toEqual(savedGroceryList);
		}]));
		
		it("removeItemFromList test - Middle Item", inject(['groceryListService', function(groceryListService) {
			expect(groceryListService).toBeDefined();
			
			var newGroceryList = [];
			var newItem1 = {id: 1, 'name': 'Canola Oil', 'hasGotten': false };
			var newItem2 = {id: 2, 'name': 'Asparagus', 'hasGotten': false };
			var newItem3 = {id: 3, 'name': 'Pulled Pork', 'hasGotten': false };
			newGroceryList.push(newItem1);
			newGroceryList.push(newItem3);
			
			groceryListService.addItemToList(newItem1.name);
			groceryListService.addItemToList(newItem2.name);
			groceryListService.addItemToList(newItem3.name);
			var actual = groceryListService.removeItemFromList(newItem2.id);
			$scope.$apply();
			
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(true);
			expect(newGroceryList).toEqual(savedGroceryList);
		}]));
		
		it("removeItemFromList test - Last Item", inject(['groceryListService', function(groceryListService) {
			expect(groceryListService).toBeDefined();
			
			var newGroceryList = [];
			var newItem1 = {id: 1, 'name': 'Canola Oil', 'hasGotten': false };
			var newItem2 = {id: 2, 'name': 'Asparagus', 'hasGotten': false };
			var newItem3 = {id: 3, 'name': 'Pulled Pork', 'hasGotten': false };
			newGroceryList.push(newItem1);
			newGroceryList.push(newItem2);
			
			groceryListService.addItemToList(newItem1.name);
			groceryListService.addItemToList(newItem2.name);
			groceryListService.addItemToList(newItem3.name);
			var actual = groceryListService.removeItemFromList(newItem3.id);
			$scope.$apply();
			
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(true);
			expect(newGroceryList).toEqual(savedGroceryList);
		}]));
		
		it("removeItemFromList test - Remove All Items", inject(['groceryListService', function(groceryListService) {
			expect(groceryListService).toBeDefined();
			
			var newGroceryList = [];
			var newItem1 = {id: 1, 'name': 'Canola Oil', 'hasGotten': false };
			var newItem2 = {id: 2, 'name': 'Asparagus', 'hasGotten': false };
			var newItem3 = {id: 3, 'name': 'Pulled Pork', 'hasGotten': false };
			
			groceryListService.addItemToList(newItem1.name);
			groceryListService.addItemToList(newItem2.name);
			groceryListService.addItemToList(newItem3.name);
			groceryListService.removeItemFromList(newItem1.id);
			groceryListService.removeItemFromList(newItem2.id);
			var actual = groceryListService.removeItemFromList(newItem3.id);
			$scope.$apply();
			
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(true);
			expect(newGroceryList).toEqual(savedGroceryList);
		}]));
		
		it("removeItemFromList test - Item Not in List", inject(['groceryListService', function(groceryListService) {
			expect(groceryListService).toBeDefined();
			
			var newGroceryList = [];
			var newItem1 = {id: 1, 'name': 'Canola Oil', 'hasGotten': false };
			var newItem2 = {id: 2, 'name': 'Asparagus', 'hasGotten': false };
			var newItem3 = {id: 3, 'name': 'Pulled Pork', 'hasGotten': false };
			newGroceryList.push(newItem1);
			newGroceryList.push(newItem2);
			newGroceryList.push(newItem3);
			
			groceryListService.addItemToList(newItem1.name);
			groceryListService.addItemToList(newItem2.name);
			groceryListService.addItemToList(newItem3.name);
			var actual = groceryListService.removeItemFromList(1234);
			$scope.$apply();
			
			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value).toEqual(false);
			expect(newGroceryList).toEqual(savedGroceryList);
		}]));
	});
	
	describe('onSaleService', function() {
		var scope, httpBackend, createController;
		var adsURL = 'http://localhost:7225/Ads';

		beforeEach(inject(function($rootScope, $httpBackend) {
			httpBackend = $httpBackend;
			scope = $rootScope.$new();
		}));

		afterEach(function() {
			httpBackend.verifyNoOutstandingExpectation();
			httpBackend.verifyNoOutstandingRequest();
		});
	
		it('getItemsOnSale test - Returns List',  inject(['onSaleService', function(onSaleService) {
			var saleItems = ['Chips', 'Soda', 'Chef Boyardee'];
			httpBackend.when('GET', adsURL).respond(200, saleItems);

			var actual = onSaleService.getItemsOnSale();
			scope.$apply();
			httpBackend.flush();

			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value.onSaleItems).toEqual(saleItems);
			expect(actual.$$state.value.errorStatus).toEqual(0);
		}]));
		
		it('getItemsOnSale test - Returns Error',  inject(['onSaleService', function(onSaleService) {
			var saleItems = ['Chips', 'Soda', 'Chef Boyardee'];
			httpBackend.when('GET', adsURL).respond(500, 'Oh no!');

			var actual = onSaleService.getItemsOnSale();
			scope.$apply();
			httpBackend.flush();

			expect(actual).toBeDefined();
			expect(actual.$$state.status).toEqual(1);
			expect(actual.$$state.value.onSaleItems).toEqual([]);
			expect(actual.$$state.value.errorStatus).toEqual(500);
		}]));
	});
});