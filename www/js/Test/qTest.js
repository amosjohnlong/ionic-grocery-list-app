describe('General AngularJS Tests', function() {
	describe('Testing $q Directly', function () {
		var deferred;
		var $rootScope;

		beforeEach(inject(function(_$q_, _$rootScope_) {
			deferred = _$q_.defer();
			$rootScope = _$rootScope_;
		}));

		it('should resolve promise', function () { 
			var response;
			deferred.promise.then(function(data) { response = data;	});

			deferred.resolve('Returned OK!');
			$rootScope.$apply();

			expect(response).toBe('Returned OK!');
		});

		it('should reject promise', function () {
			var response;
			deferred.promise.catch(function(data) { response = data; });

			deferred.reject('There has been an Error!');
			$rootScope.$apply();

			expect(response).toBe('There has been an Error!');
		});
	});
});