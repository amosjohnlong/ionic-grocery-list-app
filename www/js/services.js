app.service('onSaleService', function($http) {
	return {
		getItemsOnSale: function() { 
			var saleItemsResponse = {onSaleItems: [], errorStatus: 0};

			return $http.get('http://localhost:7225/Ads')
			.then(function(response) {
				saleItemsResponse.onSaleItems = response.data;
				return saleItemsResponse;
			}, function(jqXHR, textStatus, errorThrown) {
				saleItemsResponse.errorStatus = jqXHR.status;
				return saleItemsResponse;
			});
		},
	}
});

app.service('groceryListService', function(cookieRepositoryService) {
	var groceries = [];
					
	return {
		getGroceryList: function() { 
			return cookieRepositoryService.retrieveFromRepository('groceries')
			.then(function(object) {
				groceries = object;
				return groceries;
			});
		},
		addItemToList: function(name) {
			maxId = 1;
			for(i = 0; i < groceries.length; i++) {
				if (groceries[i].id >= maxId) { 
					maxId = groceries[i].id + 1; 
				}
			}
			
			newItem = {'id': maxId, 'name': name, 'hasGotten': false };
			groceries.push(newItem);
			
			return cookieRepositoryService.saveToRepository('groceries', groceries, null)
			.then(function() { return true;	});
		},
		removeItemFromList: function(idToRemove) {
			wasRemoved = false;
			indexToRemove = -1;
			for(i = 0; i < groceries.length; i++) {
				if (groceries[i].id == idToRemove) { 
					indexToRemove = i;
					break;
				}
			}
			
			if (indexToRemove != -1) {
				groceries.splice(indexToRemove, 1);
				wasRemoved = true;
			}
			
			return cookieRepositoryService.saveToRepository('groceries', groceries, null)
			.then(function() { return wasRemoved; });
		},
	}
});

app.service('localRepositoryService', function($q) {
	return {
		saveToRepository: function(key, javascriptObject) {
			JSONString = JSON.stringify(javascriptObject);
			localStorage.setItem(key, JSONString);
			return $q.resolve();
		},
		retrieveFromRepository: function(key) {
			JSONString = localStorage.getItem(key);
			return $q.resolve(JSON.parse(JSONString));
		},
	}
});

app.service('cookieRepositoryService', function($cookies, $q) {
	const JAN_1_3000 = "January 1, 3000";
	
	return {
		saveToRepository: function(key, javascriptObject, expirationDate) {
			JSONString = JSON.stringify(javascriptObject);
			expiresString = (expirationDate) ? expirationDate: new Date(JAN_1_3000);
			$cookies.put(key, JSONString, `expires=${expiresString}; path=/`);
			return $q.resolve();
		},
		retrieveFromRepository: function(key) {
			JSONString = $cookies.get(key);
			javascriptObject = JSON.parse(JSONString);
			return $q.resolve(javascriptObject);
		},
	}
});
