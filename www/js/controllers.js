var app = angular.module('starter.controllers', []);

app.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicLoading) {

	// In order to get feedback from the splash screen, it must be shown when the App loads ...
	$ionicLoading.show({ templateUrl: 'templates/splashScreen.html'	});
})

.controller('SplashCtrl', function($scope, $ionicLoading) {

	// ... and closed after the screen's controller is loaded
	$scope.hide = function(){
		$ionicLoading.hide();
	};
})

.controller('GroceryListCtrl', function($scope, $ionicPopover, $ionicModal, groceryListService, cookieRepositoryService) {
	$scope.isAdding = false;
	$scope.groceries = [];
	
	// Since there are two menu options that each create an instance of the controller, 
	//   you must update the grocery list every time the page becomes active
	$scope.$on('$ionicView.enter', function(e) {
		$scope.UpdateGroceryList();
	});

	$scope.UpdateGroceryList = function() {
		groceryListService.getGroceryList()
		.then(function(groceryList) {
			$scope.groceries = groceryList;
		});
	}
						
	$ionicPopover.fromTemplateUrl('templates/newGroceryItemPopover.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(popover) {
		$scope.popover = popover;
	});
	$scope.openPopover = function($event) {	$scope.popover.show($event); };
	$scope.closePopover = function() { $scope.popover.hide(); };
	
	$ionicModal.fromTemplateUrl('templates/newGroceryItemModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal = modal;
	});
	$scope.openModal = function() {	$scope.modal.show(); };
	$scope.closeModal = function() { $scope.modal.hide(); };
	
	$ionicModal.fromTemplateUrl('templates/deleteGroceryItemModal.html', {
		scope: $scope
	}).then(function(modal) {
		$scope.modal2 = modal;
	});
	$scope.openModal2 = function() { $scope.modal2.show(); };
	$scope.closeModal2 = function() { $scope.modal2.hide();	};
	
	$scope.SetIsAddingToTrue = function() { $scope.isAdding = true;	}
	$scope.SetIsAddingToFalse = function() { $scope.isAdding = false; }
	$scope.GetIsAdding = function() { return $scope.isAdding; }

	$scope.AddItem = function(name) {
		groceryListService.addItemToList(name)
		.then(function(wasSuccessful) {
			$scope.UpdateGroceryList();
		});
	}
	
	$scope.RemoveItem = function(grocery) {
		groceryListService.removeItemFromList(grocery.id)
		.then(function(wasSuccessful) {
			$scope.UpdateGroceryList();
		});
	}
})

.controller('OnSaleCtrl', function($scope, onSaleService) {
	const SERVICE_NOT_RESPONDING_MESSAGE = 'The service providing this data is not responding right now.  Please try again later.';
	
	$scope.saleItems = [];
	$scope.saleItemErrorMessage = '';
	
	$scope.$on('$ionicView.enter', function(e) {
		$scope.UpdateSaleItems();
	});
	
	$scope.UpdateSaleItems = function() {
		onSaleService.getItemsOnSale()
		.then(function(saleItemsResponse) {
			$scope.saleItems = saleItemsResponse.onSaleItems;
			$scope.saleItemErrorMessage = (saleItemsResponse.errorStatus == 0) ? '' : SERVICE_NOT_RESPONDING_MESSAGE;
		});
	}
})
